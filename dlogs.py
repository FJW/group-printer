#! /usr/bin/python3

import sys
from termcolor import colored, cprint
from collections import defaultdict
from typing import Callable, Set, List, Optional, Dict


# Shorter than a proper Miller-Rabin-implementation and still
# fast enough for the purposes for which it is used here:
def is_prime(n: int) -> int:
    if n < 2:
        return False
    elif n == 2:
        return True
    elif n % 2 == 0:
        return False
    max = int(n**0.5) + 1
    for i in range(3, max, 2):
        if n % i == 0:
            return False
    return True


class group:
    mod: int
    neutral_element: int
    order: int
    f: Optional[Callable[[int, int], int]]
    mlen: int

    def __init__(self, mod: int, neutral_element: int, order: int,
                 f: Callable[[int, int], int]) -> None:
        self.mod = mod
        self.neutral_element = neutral_element
        self.order = order
        self.f = f
        self.mlen = len(str(mod))

    def op(self, l: int, r: int) -> int:
        assert self.f is not None
        return self.f(l, r)


def print_group(g: int, grp: group) -> int:
    s = ""
    e = grp.neutral_element
    order = 0
    seen: Set[int] = set()
    last = None
    while e not in seen:
        s += f"{e:{grp.mlen}}, "
        seen.add(e)
        order += 1
        e = grp.op(e, g)
    for i in range(order, grp.order):
        s += colored(f"{e:{grp.mlen}}, ", "yellow")
        e = grp.op(e, g)

    cprint(f"{g:{grp.mlen}}: ", attrs=["bold"], end="")
    cprint(
        f"#{order:{grp.mlen}}: ",
        ("green" if is_prime(order) else "red"),
        end="")
    cprint(s)
    return order


def print_groups(grp: group) -> None:
    cprint(f"ℤ/{grp.mod}ℤ:", "red", "on_white")
    orders: Dict[int, int] = defaultdict(lambda: 0)
    for i in range(grp.neutral_element, grp.mod):
        order = print_group(i, grp)
        orders[order] += 1
    for order, num_generators in sorted(orders.items()):
        print(
            f"order {order:{grp.mlen}}: {num_generators:{grp.mlen}} element(s)")
    print("")


def main(args: List[str]) -> int:
    for arg in args[1:]:
        try:
            mod = int(arg)
            if arg[0] == '+':
                print_groups(group(mod, 0, mod, lambda x, y: (x + y) % mod))
            else:
                print_groups(
                    group(mod, 1, mod - 1, lambda x, y: (x * y) % mod))
        except ValueError as e:
            print(f"Error: {e}")
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
